'use strict';


// Selecting elements
const player0El = document.querySelector('.player--0');
const player1El = document.querySelector('.player--1');
const score0El = document.getElementById('score--0');
const score1El = document.getElementById('score--1');
const diceEl = document.querySelector('.dice');
const current0El = document.getElementById('current--0');
const current1El = document.getElementById('current--1');
const btnNew = document.querySelector('.btn--new');
const btnRoll = document.querySelector('.btn--roll');
const btnHold = document.querySelector('.btn--hold');

let scores, currentScore, activePlayer, playing; // playing is a state variable, validating the game if it's active in playing


// Initial Condition || Default Condition
const init = () => {
    // Initialize || Reset scores and current scores
    scores = [0, 0];
    currentScore = 0;
    activePlayer = 0;
    // Set playing state to true
    playing = true;

    // Initialize || Reset player names and scores in the DOM
    score0El.textContent = 0;
    score1El.textContent = 0;
    current0El.textContent = 0;
    current1El.textContent = 0;

    // Initialize || Reset player classes and active player
    player0El.classList.remove('player--winner');
    player1El.classList.remove('player--winner');
    player0El.classList.add('player--active');
    player1El.classList.remove('player--active');
    activePlayer = 0;

    // Hide the trophy emojis
    const trophies = document.querySelectorAll('.trophy');
    trophies.forEach((trophy) => {
        trophy.classList.add('hidden');
    });

    // Hide the dice
    diceEl.classList.add('hidden');
}

// Invoke the Initial || Default Condition Function
init();

// Function to Switch Player
const switchPlayer = () => {
    document.getElementById(`current--${activePlayer}`).textContent = 0;
    currentScore = 0;
    activePlayer = activePlayer === 0 ? 1 : 0;

    // Validate the background of Active Player
    player0El.classList.toggle('player--active');
    player1El.classList.toggle('player--active');
}

// FUnction Button Rolling Dice
btnRoll.addEventListener('click', () => {
    if(playing){
        // 1. Generate random dice roll
        const dice = Math.trunc(Math.random() * 6) + 1;

        // 2. Display the dice
        diceEl.classList.remove('hidden');
        diceEl.src = `./assets/img/dice-${dice}.png`;

        // 3. Validate if rolled 1
        if(dice !== 1){
            // Add dice to current score
            currentScore += dice;
            document.getElementById(`current--${activePlayer}`).textContent = currentScore;
            
        } else{ // Switch to the next player
            switchPlayer();
        }
    }
});

// Function Button Hold Score
btnHold.addEventListener('click', () => {

    if(playing){
        // 1. Add current score to active player's score
        scores[activePlayer] += currentScore;
        document.getElementById(`score--${activePlayer}`).textContent = scores[activePlayer];

        // 2. Validate if player's score is >= 100
        if(scores[activePlayer] >= 100) {
            // Finish the game
            playing = false;
            diceEl.classList.add('hidden');
            document.querySelector(`.player--${activePlayer}`).classList.add('player--winner');
            document.querySelector(`.player--${activePlayer}`).classList.remove('player--active');
            document.querySelector(`.player--${activePlayer} .trophy`).classList.remove('hidden');

        } else { // Switch to the next player
            switchPlayer();
        }
    }
});

// Function Button New Game || Reset the game
btnNew.addEventListener('click', init);